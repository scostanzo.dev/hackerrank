# https://www.hackerrank.com/challenges/python-division/problem?isFullScreen=true

def hr(a: int, b: int) -> tuple[int, float]:
    return int(a/b), a/b
