from division import hr


class Test_division:
    def test_division(self):
        test_cases = [
            {
                'name': 'test1',
                'input': [3, 5],
                'want': [0, 0.6]
            },
{
                'name': 'test2',
                'input': [4, 3],
                'want': [1, 1.3333333333333333]
            }
        ]
        for test in test_cases:
            res_int, res_float = hr(test['input'][0], test['input'][1])
            print(test['name'])
            assert res_int == test['want'][0]
            assert res_float == test['want'][1]
