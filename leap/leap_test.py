from leap import is_leap


class Test_leap:
    def test_leap(self):
        test_cases = [
            {
                "name": "2000 is leap",
                "input": 2000,
                "want": True
            },
            {
                "name": "2400 is leap",
                "input": 2400,
                "want": True
            },
            {
                "name": "1900 is not leap",
                "input": 1900,
                "want": False
            },
            {
                "name": "2100 is not leap",
                "input": 2100,
                "want": False
            },
            {
                "name": "1990 is not leap",
                "input": 1990,
                "want": False
            },
            {
                "name": "1996 is leap",
                "input": 1996,
                "want": True
            },
        ]
        for test in test_cases:
            got = is_leap(test['input'])
            print(test['name'])
            assert got == test['want']
