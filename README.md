[![pipeline status](https://gitlab.com/scostanzo.dev/hackerrank/badges/main/pipeline.svg)](https://gitlab.com/scostanzo.dev/hackerrank/-/commits/main)

[![coverage report](https://gitlab.com/scostanzo.dev/hackerrank/badges/main/coverage.svg)](https://gitlab.com/scostanzo.dev/hackerrank/-/commits/main)

[![Latest Release](https://gitlab.com/scostanzo.dev/hackerrank/-/badges/release.svg)](https://gitlab.com/scostanzo.dev/hackerrank/-/releases)
# HackerRank exercises

Some exercises by HackerRank for the Python path.
I had fun by adding Unit Tests using TDD approach and also by using static type.

Some solutions are not exactly the same as the HackerRank output since I had to adapt them in order to pass my Unit Tests.
For example HackerRank expects to *print* the result while in my code I *return* the result.
