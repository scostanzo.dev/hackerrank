# https://www.hackerrank.com/challenges/py-if-else/problem

def hr(input: int) -> str:
    if input % 2 == 0 and (input > 20 or (input > 1 and input < 6)):
        return 'Not Weird'
    else:
        return 'Weird'