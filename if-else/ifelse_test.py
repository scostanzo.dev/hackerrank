from ifelse import hr

class Test_ifelse:
    def test_ifelse(self):
        test_cases = [
            {
                "name": "input is odd",
                "input": 3,
                "want": "Weird"
            },
            {
                "name": "input is even and in the inclusive range of 2 to 5",
                "input": 2,
                "want": "Not Weird"
            },
            {
                "name": "input is even and in the inclusive range of 6 to 20",
                "input": 6,
                "want": "Weird"
            },
            {
                "name": "input is even and greater than 20",
                "input": 22,
                "want": "Not Weird"
            }
        ]
        for test in test_cases:
            got = hr(test['input'])
            print(test['name'])
            assert got == test["want"]
